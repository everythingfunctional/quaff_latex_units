module quaff_latex_unit_utilities_m
    use iso_varying_string, only: varying_string, operator(//), var_str
    use parff, only: parser_output_t, state_t, parse_char, parse_string

    implicit none
    private

    interface wrap_in_latex_quantity
        module procedure wrap_in_latex_quantity_cc
        module procedure wrap_in_latex_quantity_cs
        module procedure wrap_in_latex_quantity_sc
        module procedure wrap_in_latex_quantity_ss
    end interface

    interface wrap_in_latex_unit
        module procedure wrap_in_latex_unit_c
        module procedure wrap_in_latex_unit_s
    end interface

    public :: &
            parse_close_brace, &
            parse_open_brace, &
            parse_si, &
            wrap_in_latex_quantity, &
            wrap_in_latex_unit
contains
    function parse_close_brace(state_) result(result_)
        type(state_t), intent(in) :: state_
        type(parser_output_t) :: result_

        result_ = parse_char("}", state_)
    end function

    function parse_open_brace(state_) result(result_)
        type(state_t), intent(in) :: state_
        type(parser_output_t) :: result_

        result_ = parse_char("{", state_)
    end function

    function parse_si(state_) result(result_)
        type(state_t), intent(in) :: state_
        type(parser_output_t) :: result_

        result_ = parse_string("\SI", state_)
    end function

    pure function wrap_in_latex_quantity_cc(number, units) result(latex_command)
        character(len=*), intent(in) :: number
        character(len=*), intent(in) :: units
        type(varying_string) :: latex_command

        latex_command = wrap_in_latex_quantity(var_str(number), var_str(units))
    end function

    pure function wrap_in_latex_quantity_cs(number, units) result(latex_command)
        character(len=*), intent(in) :: number
        type(varying_string), intent(in) :: units
        type(varying_string) :: latex_command

        latex_command = wrap_in_latex_quantity(var_str(number), units)
    end function

    pure function wrap_in_latex_quantity_sc(number, units) result(latex_command)
        type(varying_string), intent(in) :: number
        character(len=*), intent(in) :: units
        type(varying_string) :: latex_command

        latex_command = wrap_in_latex_quantity(number, var_str(units))
    end function

    pure function wrap_in_latex_quantity_ss(number, units) result(latex_command)
        type(varying_string), intent(in) :: number
        type(varying_string), intent(in) :: units
        type(varying_string) :: latex_command

        latex_command = "\SI{" // number // "}{" // units // "}"
    end function

    pure function wrap_in_latex_unit_c(units) result(latex_command)
        character(len=*), intent(in) :: units
        type(varying_string) :: latex_command

        latex_command = wrap_in_latex_unit(var_str(units))
    end function

    pure function wrap_in_latex_unit_s(units) result(latex_command)
        type(varying_string), intent(in) :: units
        type(varying_string) :: latex_command

        latex_command = "\si{" // units // "}"
    end function
end module
