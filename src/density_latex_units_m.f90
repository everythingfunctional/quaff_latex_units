module density_latex_units_m
    use erloff, only: error_list_t, fatal_t, module_t, procedure_t
    use iso_varying_string, only: varying_string, assignment(=), operator(//)
    use parff, only: &
            parse_result_t, &
            parsed_rational_t, &
            parser_output_t, &
            state_t, &
            drop_then, &
            parse_rational, &
            parse_string, &
            parse_with, &
            then_drop
    use quaff_conversion_factors_m, only: &
            GRAMS_PER_CUBIC_METER_PER_KILOGRAMS_PER_CUBIC_METER
    use quaff_latex_unit_utilities_m, only: &
            parse_close_brace, &
            parse_open_brace, &
            parse_si, &
            wrap_in_latex_quantity, &
            wrap_in_latex_unit
    use quaff_utilities_m, only: PARSE_ERROR
    use density_m, only: &
            density_unit_t, &
            fallible_density_t, &
            operator(.unit.)

    implicit none
    private
    public :: &
            density_latex_unit_t, &
            PROVIDED_UNITS, &
            GRAMS_PER_CUBIC_METER, &
            KILOGRAMS_PER_CUBIC_METER

    type, extends(density_unit_t), public :: density_latex_unit_t
        character(len=100) :: symbol
    contains
        procedure :: unit_to_string
        procedure :: value_to_string
        procedure :: parse_as
    end type

    type(density_latex_unit_t), parameter :: GRAMS_PER_CUBIC_METER = &
            density_latex_unit_t( &
                    conversion_factor = GRAMS_PER_CUBIC_METER_PER_KILOGRAMS_PER_CUBIC_METER, &
                    symbol = "\gram\per\cubic\meter")
    type(density_latex_unit_t), parameter :: KILOGRAMS_PER_CUBIC_METER = &
            density_latex_unit_t( &
                    conversion_factor = 1.0d0, &
                    symbol = "\kilo\gram\per\cubic\meter")

    type(density_latex_unit_t), parameter :: PROVIDED_UNITS(*) = &
            [ GRAMS_PER_CUBIC_METER &
            , KILOGRAMS_PER_CUBIC_METER &
            ]
contains
    elemental function unit_to_string(self) result(string)
        class(density_latex_unit_t), intent(in) :: self
        type(varying_string) :: string

        string = wrap_in_latex_unit(trim(self%symbol))
    end function

    pure function value_to_string(self, value_) result(string)
        class(density_latex_unit_t), intent(in) :: self
        type(varying_string), intent(in) :: value_
        type(varying_string) :: string

        string = wrap_in_latex_quantity(value_, trim(self%symbol))
    end function

    function parse_as(self, string) result(fallible_density)
        class(density_latex_unit_t), intent(in) :: self
        type(varying_string), intent(in) :: string
        type(fallible_density_t) :: fallible_density

        type(parse_result_t) :: parse_result

        parse_result = parse_with(the_parser, string)
        if (parse_result%ok()) then
            select type (the_number => parse_result%parsed())
            type is (parsed_rational_t)
                fallible_density = fallible_density_t(the_number%value_().unit.self)
            end select
        else
            fallible_density = fallible_density_t(error_list_t(fatal_t( &
                    PARSE_ERROR, &
                    module_t("density_latex_units_m"), &
                    procedure_t("parse_as"), &
                    parse_result%message())))
        end if
    contains
        function the_parser(state_) result(result_)
            type(state_t), intent(in) :: state_
            type(parser_output_t) :: result_

            result_ = then_drop( &
                    then_drop( &
                            then_drop( &
                                    then_drop( &
                                            drop_then( &
                                                    drop_then(parse_si, parse_open_brace, state_), &
                                                    parse_rational), &
                                            parse_close_brace), &
                                    parse_open_brace), &
                            parse_unit), &
                    parse_close_brace)
        end function

        function parse_unit(state_) result(result_)
            type(state_t), intent(in) :: state_
            type(parser_output_t) :: result_

            result_ = parse_string(trim(self%symbol), state_)
        end function
    end function
end module
